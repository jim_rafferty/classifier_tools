
# pylint: disable=invalid-name
# pylint: disable=no-member

from __future__ import print_function
import time # not currently doing anything, but will add some extra output in future.
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data

import numpy as np

class BaseClassifierObject(object):
    """Base class from which classifier objects inherit basic methods"""
    def __init__(self, n_weights=None, learn_rate=0.01,
                 batch_size=100, n_steps=2000, verbose=True, **kwargs):
        """method for creating an instance of the classifier object
        Inputs:
        n_weights: a 1D array with the number of weights at each layer of the
        network. There can be any number of layers.
        learn_rate: the learning rate used in the optimiser.
        batch_size: the number of samples used in each step of the training.
        n_steps: the number of training steps to perform.
        verbose: display information during training."""
        self.N_WEIGHTS = n_weights
        self.LEARN_RATE = learn_rate
        self.BATCH_SIZE = batch_size
        self.N_STEPS = n_steps
        self.VERBOSE = verbose
        self.MODEL = {"trained": False}
        self._additional__init__(kwargs)

    def __str__(self):
        """What is printed when print(class) is called"""
        s = self.__class__.__name__ + "("
        for ii in dir(self):
            if not callable(getattr(self, ii)) and not ii.startswith("__"):
                s += ii + "=" + str(getattr(self, ii)) + ", "
        s += ")"
        return s

    def _additional__init__(self, kwargs):
        """placeholder for any additional initialisation that a child class
        may require"""
        pass
    def _construct_model(self, images):
        """placeholder for contruction of the neural network"""
        pass
    def _optimise(self, predictor, labels):
        """placeholder for optimisation function of the NN"""
        pass
    def _construct_feed_dict(self, data, labels=None, training=True):
        """Placeholder for the construction of the feed dict"""
        pass
    def _evaluation(self, predictions, labels):
        """Internal method: evaluates the model against the provided labels"""
        correct = tf.equal(tf.argmax(predictions, 1),
                           tf.cast(labels, tf.int64))
        return tf.reduce_sum(tf.cast(correct, tf.int64))
    def fit(self, data, labels):
        """method for training the model.
        Inputs:
        data - a n_samples x n_features array of training data.
        labels - a n_samples sized array of labels for the training data.
        """
        assert self.N_WEIGHTS is not None, ("Number of weights per layer must"+
                                            " be specified")
        n_samples = np.shape(data)[0]
        self.MODEL["n_features"] = np.prod(np.shape(data)[1:])
        self.MODEL["feature_shape"] = list(np.shape(data)[1:])
        self.MODEL["n_classes"] = len(np.unique(labels))
        ###

        self.MODEL["images_placeholder"] = tf.placeholder(tf.float32,
                                                          shape=tuple([self.BATCH_SIZE]
                                                                      +self.MODEL["feature_shape"]))
        self.MODEL["labels_placeholder"] = tf.placeholder(tf.int32,
                                                          shape=(self.BATCH_SIZE))

        self.MODEL["predictor"] = self._construct_model(self.MODEL["images_placeholder"])
        opt_obj, loss_value = self._optimise(self.MODEL["predictor"],
                                             self.MODEL["labels_placeholder"])
        self.MODEL["n_correct"] = self._evaluation(self.MODEL["predictor"],
                                                   self.MODEL["labels_placeholder"])

        self.MODEL["sess"] = tf.Session()
        init = tf.initialize_all_variables()
        self.MODEL["sess"].run(init)
        rand_order = np.random.permutation(n_samples)
        data = data[rand_order]
        labels = labels[rand_order]# this takes about 1/5 sec with 55000 samples
        start = 0
        if self.VERBOSE:
            print("Training")
        for kk in xrange(self.N_STEPS):
            train_block = data[start:(start + self.BATCH_SIZE), :]
            train_label_block = labels[start:(start + self.BATCH_SIZE)]
            start = (start + self.BATCH_SIZE) % (n_samples - self.BATCH_SIZE)
            feed_dict = self._construct_feed_dict(train_block, train_label_block)
            summary = self.MODEL["sess"].run([opt_obj, loss_value],
                                             feed_dict=feed_dict)
            if (kk % (self.N_STEPS / 10) == 0 and self.VERBOSE) or kk == self.N_STEPS - 1:
                print("Step", kk, "Loss value:", summary[1])
                correct_count = 0
                for ii in xrange(0, n_samples/self.BATCH_SIZE):
                    test_block = data[ii * self.BATCH_SIZE:
                                      (ii + 1) * self.BATCH_SIZE, :]
                    test_block_labels = labels[ii * self.BATCH_SIZE:
                                               (ii + 1) * self.BATCH_SIZE]
                    feed_dict = self._construct_feed_dict(test_block, test_block_labels,
                                                          training=False)
                    correct_count += self.MODEL["sess"].run(self.MODEL["n_correct"],
                                                            feed_dict=feed_dict)


                print("Total samples =", n_samples)
                print("Correct samples =", correct_count)
                print("In sample error =",
                      float(n_samples - correct_count)/ n_samples)
                print("")
            self.MODEL["trained"] = True
    def predict(self, data, labels=None):
        """method for generating predictions from a trained network
        Inputs:
        data - a n_samples x n_features array of data.
        labels - a n_samples sized array of labels (optional)."""

        n_samples = np.shape(data)[0]
        results = []
        correct_count = 0
        for ii in xrange(0, n_samples/self.BATCH_SIZE):
            test_block = data[ii * self.BATCH_SIZE:
                              (ii + 1) * self.BATCH_SIZE, :]
            if labels is not None:
                test_block_labels = labels[ii * self.BATCH_SIZE:
                                           (ii + 1) * self.BATCH_SIZE]
                feed_dict = self._construct_feed_dict(test_block, test_block_labels, training=False)
            else:
                feed_dict = self._construct_feed_dict(test_block, training=False)
            summary = self.MODEL["sess"].run(self.MODEL["predictor"],
                                             feed_dict=feed_dict)
            if labels is not None:
                correct_count += self.MODEL["sess"].run(
                    self.MODEL["n_correct"], feed_dict=feed_dict)
            results += np.argmax(summary, axis=1).tolist()
        if labels is not None:
            print("Testing")
            print("Total samples =", n_samples)
            print("Correct samples =", correct_count)
            print("Out of sample error =",
                  float(n_samples - correct_count)/ n_samples)
        return results

class FeedForwardNNClassifier(BaseClassifierObject):
    """ A simple feed forward neural network for classification """

    def _construct_feed_dict(self, data, labels=None, training=True):
        """Internal function for the construction of the feed dict"""
        if labels is not None:
            return {self.MODEL["images_placeholder"]: data,
                    self.MODEL["labels_placeholder"]: labels}
        else:
            return {self.MODEL["images_placeholder"]: data}

    def _construct_layer(self, images, n_features, reduced_features, count):
        """Internal function: constructs layers of the neural network"""
        with tf.name_scope("hidden%u" % (count)):
            weights = tf.Variable(tf.truncated_normal(
                [n_features, reduced_features],
                stddev=1.0/tf.sqrt(float(n_features))),
                                  name="weights")
            biases = tf.Variable(tf.zeros([reduced_features]), name="biases")
            hidden = tf.nn.relu(tf.matmul(images, weights) + biases)
            return hidden

    def _construct_model(self, images):
        """Internal method: loops over __construct_layer to create the
        neural network"""
        n_weight_array = ([self.MODEL["n_features"]] + self.N_WEIGHTS +
                          [self.MODEL["n_classes"]])
        layer = images
        for ii in range(len(n_weight_array) - 1):
            layer = self._construct_layer(layer, n_weight_array[ii],
                                          n_weight_array[ii + 1], ii)

        return layer # the predictor - no knowledge of labels.

    def _optimise(self, predictor, labels):
        """Internal method: defines the loss function and optimiser"""
        # loss function
        with tf.name_scope("loss"):
            labels = tf.to_int64(labels)
            cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(
                predictor, labels, name="xentropy")
            loss = tf.reduce_mean(cross_entropy,
                                  name="xentropy_mean")
        optimiser = tf.train.GradientDescentOptimizer(self.LEARN_RATE)
        train_op = optimiser.minimize(loss)
        return train_op, loss

class ConvolutionalNNClassifier(BaseClassifierObject):
    """ A simple convolutional neural network for classification """
    def _additional__init__(self, kwargs):
        """Additional initialisation required for CNN classifier:
        dropout - the probability that a training sample is used in the
        optimisation. Reducing this number reduces the chances of
        overfitting.
        patch_size - the inital size of each patch"""
        if "dropout" in kwargs.keys():
            self.MODEL["dropout_probability"] = kwargs["dropout"]
        else:
            self.MODEL["dropout_probability"] = 1.0
        if "patch_size" in kwargs.keys():
            self.MODEL["patch_size"] = kwargs["patch_size"]
        else:
            self.MODEL["patch_size"] = 5

    def _construct_feed_dict(self, data, labels=None, training=True):
        """Internal function for the construction of the feed dict"""
        if training:
            keep_prob = self.MODEL["dropout_probability"]
        else:
            keep_prob = 1.0
        if labels is not None:
            return {self.MODEL["images_placeholder"]: data,
                    self.MODEL["labels_placeholder"]: labels,
                    self.MODEL["keep_prob"]: keep_prob}
        else:
            return {self.MODEL["images_placeholder"]: data,
                    self.MODEL["keep_prob"]: keep_prob}

    def _construct_layer(self, images, n_weights, n_weights_next, count):
        with tf.name_scope("layer%u" % (count)):
            shape = [self.MODEL["patch_size"], self.MODEL["patch_size"], n_weights, n_weights_next]
            weights = tf.Variable(tf.truncated_normal(shape, stddev=0.1))
            bias = tf.Variable(tf.constant(0.1, shape=[n_weights_next]))
            conv1 = tf.nn.relu(tf.nn.conv2d(
                images, weights, strides=[1, 1, 1, 1], padding='SAME') + bias)
            pool1 = tf.nn.max_pool(conv1, ksize=[1, 2, 2, 1],
                                   strides=[1, 2, 2, 1], padding='SAME')
        return pool1

    def _construct_model(self, images):
        """placeholder for contruction of the neural network"""

        n_weights_array = [1] + self.N_WEIGHTS
        # need to know how many elements there will be after the conv layers have been applied.
        n_conv_layers = len(n_weights_array) - 2
        size_after_conv = np.array(map(float, self.MODEL["feature_shape"])) / 2 ** n_conv_layers
        size_after_conv = map(int, [np.ceil(jj) for jj in size_after_conv if jj > 0])
        layer = images
        # loop to construct conv layers
        for ii in xrange(len(n_weights_array) - 2):
            layer = self._construct_layer(layer, n_weights_array[ii], n_weights_array[ii+1], ii)
        # then construct connected and readout layers
        with tf.name_scope("connected_layer"):
            weights = tf.Variable(tf.truncated_normal(
                [np.prod(size_after_conv) * n_weights_array[-2], n_weights_array[-1]], stddev=0.1))
            bias = tf.Variable(tf.constant(0.1, shape=[n_weights_array[-1]]))
            pool_flat = tf.reshape(layer, [-1, np.prod(size_after_conv) * n_weights_array[-2]])
            layer = tf.nn.relu(tf.matmul(pool_flat, weights) + bias)
            # dropout
        self.MODEL["keep_prob"] = tf.placeholder(tf.float32)
        layer_drop = tf.nn.dropout(layer, self.MODEL["keep_prob"])
        with tf.name_scope("readout_layer"):
            weights = tf.Variable(tf.truncated_normal(
                [n_weights_array[-1], self.MODEL["n_classes"]], stddev=0.1))
            bias = tf.Variable(tf.constant(0.1, shape=[self.MODEL["n_classes"]]))
            y_conv = tf.nn.softmax(tf.matmul(layer_drop, weights) + bias)
        return y_conv # model prediction. Doen't know about labels.

    def _optimise(self, predictor, labels):
        """placeholder for optimisation function of the NN"""
        labels = tf.to_int64(labels)
        cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(
            predictor, labels, name="xentropy")
        loss = tf.reduce_mean(cross_entropy,
                              name="xentropy_mean")
        train_step = tf.train.AdamOptimizer(self.LEARN_RATE).minimize(cross_entropy)
        return train_step, loss

if __name__ == "__main__":


    mnist = input_data.read_data_sets("MNIST_data/")
    train_data = mnist.train.images
    train_labels = mnist.train.labels
    test_data = mnist.test.images
    test_labels = mnist.test.labels
    nn_type = "conv" # "conv" or "feed_forward"
    if nn_type == "feed_forward":
        clf = FeedForwardNNClassifier(learn_rate=0.5, n_steps=2000,
                                      n_weights=[128, 32], verbose=True)
    elif nn_type == "conv":
        clf = ConvolutionalNNClassifier(dropout=0.5, n_steps=20000, n_weights=[32, 64, 1024],
                                        learn_rate=1e-4, verbose=True)
        # convNN requires 2D images.
        train_data = np.reshape(train_data, [-1, 28, 28, 1])
        test_data = np.reshape(test_data, [-1, 28, 28, 1])
    clf.fit(train_data, train_labels)
    res = clf.predict(test_data, test_labels)

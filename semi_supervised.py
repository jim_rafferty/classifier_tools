from __future__ import print_function


class SemiSupervised(object):
    """Class for performing semi supervised learning with an sklearn object"""

    def __init__(self, clf, prob_cut=0.8, n_iters=500):
        """Initialisation
        clf - sklearn classifier (XXX or regressor?)
        prob_cut - the lower bound for unlabelled samples to be given a label.
        n_iters - the number of iterations performed. Will terminate early if 
        all unlabeled samples are labelled with the required prob_cut."""

        self.parameters = {"clf": clf,
                           "prob_cut": prob_cut,
                           "n_iters": n_iters}
        self.parameters["clf"].set_params(probability=True)        

    def fit(self, train_data, labels, unlabelled_train_data=None):
        """ Trains the classifier """
        assert unlabelled_train_data is not None, ("No unlabelled data " +
            "provided. Either use supervised algorithm or provide unlabelled" + 
            " data.")
        self.parameters["classes"] = np.unique(labels)
        train_set = train_data
        train_labels = labels 
        used_unlabelled_data = np.array([]).reshape(0, np.shape(unlabelled_train_data)[1])
        used_ul_labels = np.array([])#.reshape(0, 1)
        base_weights = np.ones((len(labels)))
        sample_weights = base_weights
        new_weights = np.array([])
        for ii in xrange(self.parameters["n_iters"]):
            # train with labelled data and last iters best unlabelled data
            if len(unlabelled_train_data) < 1:
                print("All unlabelled data used after %u iterations" % (ii))
                break
            self.parameters["clf"].fit(train_set, train_labels, sample_weights)
            res = self.parameters["clf"].predict_proba(unlabelled_train_data)
            # find the classification from the unlabelled data with the highest probability
            best_data_ind = np.argmax(np.max(res, axis=1))
            best_label_ind = np.argmax(np.max(res, axis=0))
            best_prob = np.max(np.max(res))
            #if ii == 0: # this sucks
            new_weights = np.append(new_weights, best_prob)
            if best_prob < self.parameters["prob_cut"]:
                print("Training terminated after %u iterations" % (ii))
                break
            # find the best sample and label
            best_sample = unlabelled_train_data[best_data_ind, :].reshape(-1, np.shape(train_data)[1])
            best_label = self.parameters["classes"][best_label_ind].reshape(-1)
            # append them to a new array
            used_unlabelled_data = np.concatenate((used_unlabelled_data, best_sample), axis=0)
            used_ul_labels = np.concatenate((used_ul_labels, best_label), axis=0)
            # delete them from the unlabelled data
            unlabelled_train_data = np.delete(unlabelled_train_data, best_data_ind, axis=0)            
            # calculate probabilities by leave one out.
            old_weights = np.copy(new_weights)  
            new_weights = []
            samples_to_delete = []
            for kk in xrange(np.shape(used_unlabelled_data)[0]):
                left_out_sample = used_unlabelled_data[kk, :]
                cross_validate = np.delete(used_unlabelled_data, kk, axis=0)
                sample_label = used_ul_labels[kk]
                cross_validate_labels = np.delete(used_ul_labels, kk, axis=0)                
                cross_validate_weights = np.delete(old_weights, kk)
                self.parameters["clf"].fit(np.concatenate((train_data, cross_validate), axis=0), 
                                           np.concatenate((labels, cross_validate_labels), axis=0), 
                                           np.concatenate((base_weights, cross_validate_weights), axis=0))
                probs = self.parameters["clf"].predict_proba(left_out_sample.reshape(1, -1))
                if probs[0][sample_label] < self.parameters["prob_cut"]:
                    samples_to_delete.append(kk)
                new_weights.append(probs[0][sample_label])
            if len(samples_to_delete) > 0:
                used_unlabelled_data = np.delete(used_unlabelled_data, samples_to_delete, axis=0)
                used_ul_labels = np.delete(used_ul_labels, samples_to_delete, axis=0)
                new_weights = np.delete(np.array(new_weights), samples_to_delete, axis=0)

            # update data arrays for the next iteration
            sample_weights = np.concatenate((base_weights, new_weights), axis=0)
            train_set = np.concatenate((train_data, used_unlabelled_data), axis=0)
            train_labels = np.concatenate((labels, used_ul_labels), axis=0)
            # final training ready for prediction later
        self.parameters["clf"].fit(train_set, train_labels)
        print("Samples used for training = %u" % (len(train_labels)))
        
    def predict(self, test_data):
        return self.parameters["clf"].predict(test_data)

if __name__ == "__main__": 
    from sklearn.datasets import load_iris
    from sklearn.svm import SVC
    import numpy as np
    import matplotlib.pylab as pyl
    
    data = load_iris()
    full_data = data.data
    full_targets = data.target
    
    n_features = 2
    rand_order = np.random.permutation(len(full_targets))
    full_data = full_data[rand_order, 0:n_features]
    full_targets = full_targets[rand_order]
    
    data_class = [np.array([full_data[ii] for ii in range(len(full_data)) if full_targets[ii] == jj]) for jj in range(3)]
        
    n_train_samples = 10
    n_ul_samples = 25
    
    
    train_data = np.array([]).reshape(0, n_features) # there are always 4 features in the iris set
    train_targets = []
    unlabelled_data = np.array([]).reshape(0, n_features)
    test_data = np.array([]).reshape(0, n_features)
    test_targets = []
    for ii in range(3):
        train_data = np.concatenate((train_data, data_class[ii][0:n_train_samples]), axis=0)
        train_targets = train_targets + (np.ones(n_train_samples)* ii).astype(int).tolist()
        unlabelled_data = np.concatenate((unlabelled_data, data_class[ii][
            n_train_samples:(n_train_samples + n_ul_samples)]), axis=0)
        test_data = np.concatenate((test_data, data_class[ii][(n_train_samples + n_ul_samples):50]), axis=0)
        test_targets = test_targets + (
            np.ones(50 - (n_train_samples + n_ul_samples) )* ii).astype(int).tolist()

    clf = SVC(kernel="linear")
    ssClf = SemiSupervised(clf, prob_cut=0.7)
    ssClf.fit(train_data, train_targets, unlabelled_data)
    results = ssClf.predict(test_data)
    n_correct = np.sum((results == test_targets).astype(int))
    
    clf2 = SVC(kernel="linear")
    clf2.fit(train_data, train_targets)
    res_no_unlabelled = clf2.predict(test_data)
    n_correct_noul = np.sum((res_no_unlabelled == test_targets).astype(int))
    
    print("Standard Results:\n%u out of %u (%f) correct" % (n_correct_noul, len(test_targets),
                                   float(n_correct_noul)/len(test_targets)))
                                   
    print("Results with unlabelled data:\n%u out of %u (%f) correct" % (n_correct, len(test_targets),
                                   float(n_correct)/len(test_targets)))
             
    if n_features == 2:
        color_map = {-1: (1, 1, 1), 0: (0, 0, .9), 1: (1, 0, 0), 2: (.8, .6, 0)}                      
        h = .02
        x_min, x_max = full_data[:, 0].min() - 1, full_data[:, 0].max() + 1
        y_min, y_max = full_data[:, 1].min() - 1, full_data[:, 1].max() + 1
        xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
        colors = [color_map[y] for y in full_targets]
        colors_train = [color_map[y] for y in train_targets]
        
        pyl.figure()
        pyl.subplot(1, 2, 1)
        Z = clf2.predict(np.c_[xx.ravel(), yy.ravel()])
        Z = Z.reshape(xx.shape)
        pyl.contourf(xx, yy, Z, cmap=pyl.cm.Paired)    
        pyl.scatter(full_data[:, 0], full_data[:, 1], c=colors, cmap=pyl.cm.Paired)
        pyl.scatter(train_data[:, 0], train_data[:, 1], c=colors_train, cmap=pyl.cm.Paired, marker="*", s=60)
        pyl.title("Standard Linear SVM")
        
        pyl.subplot(1, 2, 2)
        Z = ssClf.predict(np.c_[xx.ravel(), yy.ravel()])
        Z = Z.reshape(xx.shape)
        pyl.contourf(xx, yy, Z, cmap=pyl.cm.Paired)    
        pyl.scatter(full_data[:, 0], full_data[:, 1], c=colors, cmap=pyl.cm.Paired)
        pyl.scatter(train_data[:, 0], train_data[:, 1], c=colors_train, cmap=pyl.cm.Paired, marker="*", s=60)
        pyl.title("Semi supervised self trained SVM")
        
        pyl.show()
    
    